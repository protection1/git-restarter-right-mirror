Bumps [@babel/preset-env](https://github.com/babel/babel) from 7.6.3 to 7.7.6.
<details>
<summary>Release notes</summary>

*Sourced from [@babel/preset-env's releases](https://github.com/babel/babel/releases).*

> ## v7.7.6 (2019-12-08)
> 
> #### :house: Internal
> * [#10836](https://github-redirect.dependabot.com/babel/babel/pull/10836) chore: add PR Revert labels to changelog [ci-skip] ([@&#8203;JLHwung](https://github.com/JLHwung))
> 
> #### :leftwards_arrow_with_hook: Revert
> * `babel-plugin-transform-modules-commonjs`, `babel-plugin-transform-regenerator`, `babel-plugin-transform-runtime`, `babel-preset-env`, `babel-runtime-corejs2`
>   * [#10835](https://github-redirect.dependabot.com/babel/babel/pull/10835) Revert "Add ".js" extension to injected polyfill imports" ([@&#8203;JLHwung](https://github.com/JLHwung))
> 
> #### Committers: 1
> - Huáng Jùnliàng ([@&#8203;JLHwung](https://github.com/JLHwung))
> 
> ## v7.7.5 (2019-12-06)
> 
> #### :bug: Bug Fix
> * `babel-plugin-transform-modules-commonjs`, `babel-plugin-transform-regenerator`, `babel-plugin-transform-runtime`, `babel-preset-env`, `babel-runtime-corejs2`
>   * [#10549](https://github-redirect.dependabot.com/babel/babel/pull/10549) Add ".js" extension to injected polyfill imports ([@&#8203;shimataro](https://github.com/shimataro))
> * `babel-cli`
>   * [#10283](https://github-redirect.dependabot.com/babel/babel/pull/10283) `babel --watch` should have equivalent file selection logic with `babel` ([@&#8203;JLHwung](https://github.com/JLHwung))
> * `babel-parser`
>   * [#10801](https://github-redirect.dependabot.com/babel/babel/pull/10801) Use scope flags to check arguments ([@&#8203;JLHwung](https://github.com/JLHwung))
>   * [#10800](https://github-redirect.dependabot.com/babel/babel/pull/10800) Allow tuple rest trailing comma ([@&#8203;yeonjuan](https://github.com/yeonjuan))
>   * [#10475](https://github-redirect.dependabot.com/babel/babel/pull/10475) Correctly disambiguate / after async fuctions ([@&#8203;nicolo-ribaudo](https://github.com/nicolo-ribaudo))
> * `babel-parser`, `babel-plugin-proposal-optional-chaining`, `babel-plugin-transform-modules-amd`
>   * [#10806](https://github-redirect.dependabot.com/babel/babel/pull/10806) fix(optional chaining): Optional delete returns true with nullish base ([@&#8203;mpaarating](https://github.com/mpaarating))
> * `babel-helper-module-transforms`, `babel-plugin-transform-modules-amd`
>   * [#10764](https://github-redirect.dependabot.com/babel/babel/pull/10764) fix: rewriteBindingInitVisitor should skip on scopable node ([@&#8203;JLHwung](https://github.com/JLHwung))
> 
> #### :nail_care: Polish
> * `babel-plugin-transform-runtime`
>   * [#10788](https://github-redirect.dependabot.com/babel/babel/pull/10788) Do not transpile typeof helper with itself in babel/runtime ([@&#8203;nicolo-ribaudo](https://github.com/nicolo-ribaudo))
> * `babel-core`
>   * [#10778](https://github-redirect.dependabot.com/babel/babel/pull/10778) refactor: Improve error message in @babel/core ([@&#8203;jaroslav-kubicek](https://github.com/jaroslav-kubicek))
> 
> #### :house: Internal
> * `babel-preset-env-standalone`
>   * [#10779](https://github-redirect.dependabot.com/babel/babel/pull/10779) Bundle standalone using rollup ([@&#8203;JLHwung](https://github.com/JLHwung))
> * Other
>   * [#10781](https://github-redirect.dependabot.com/babel/babel/pull/10781) Tune makefile scripts ([@&#8203;JLHwung](https://github.com/JLHwung))
> * `babel-helper-transform-fixture-test-runner`
>   * [#10566](https://github-redirect.dependabot.com/babel/babel/pull/10566) Incorrect trace position in fixture runner ([@&#8203;JLHwung](https://github.com/JLHwung))
> 
> #### Committers: 8
> - Alex Lewis ([@&#8203;mpaarating](https://github.com/mpaarating))
> - Huáng Jùnliàng ([@&#8203;JLHwung](https://github.com/JLHwung))
> - Ives van Hoorne ([@&#8203;CompuIves](https://github.com/CompuIves))
> - Jaroslav Kubíček ([@&#8203;jaroslav-kubicek](https://github.com/jaroslav-kubicek))
> - Kai Cataldo ([@&#8203;kaicataldo](https://github.com/kaicataldo))
> - Nicolò Ribaudo ([@&#8203;nicolo-ribaudo](https://github.com/nicolo-ribaudo))
> - Taro Odashima ([@&#8203;shimataro](https://github.com/shimataro))
></tr></table> ... (truncated)
</details>
<details>
<summary>Changelog</summary>

*Sourced from [@babel/preset-env's changelog](https://github.com/babel/babel/blob/master/CHANGELOG.md).*

> ## v7.7.6 (2019-12-08)
> 
> #### :house: Internal
> * [#10836](https://github-redirect.dependabot.com/babel/babel/pull/10836) chore: add PR Revert labels to changelog [ci-skip] ([@&#8203;JLHwung](https://github.com/JLHwung))
> 
> #### :leftwards_arrow_with_hook: Revert
> * `babel-plugin-transform-modules-commonjs`, `babel-plugin-transform-regenerator`, `babel-plugin-transform-runtime`, `babel-preset-env`, `babel-runtime-corejs2`
>   * [#10835](https://github-redirect.dependabot.com/babel/babel/pull/10835) Revert "Add ".js" extension to injected polyfill imports" ([@&#8203;JLHwung](https://github.com/JLHwung))
> ## v7.7.5 (2019-12-06)
> 
> #### :bug: Bug Fix
> * `babel-plugin-transform-modules-commonjs`, `babel-plugin-transform-regenerator`, `babel-plugin-transform-runtime`, `babel-preset-env`, `babel-runtime-corejs2`
>   * [#10549](https://github-redirect.dependabot.com/babel/babel/pull/10549) Add ".js" extension to injected polyfill imports ([@&#8203;shimataro](https://github.com/shimataro))
> * `babel-cli`
>   * [#10283](https://github-redirect.dependabot.com/babel/babel/pull/10283) `babel --watch` should have equivalent file selection logic with `babel` ([@&#8203;JLHwung](https://github.com/JLHwung))
> * `babel-parser`
>   * [#10801](https://github-redirect.dependabot.com/babel/babel/pull/10801) Use scope flags to check arguments ([@&#8203;JLHwung](https://github.com/JLHwung))
>   * [#10800](https://github-redirect.dependabot.com/babel/babel/pull/10800) Allow tuple rest trailing comma ([@&#8203;yeonjuan](https://github.com/yeonjuan))
>   * [#10475](https://github-redirect.dependabot.com/babel/babel/pull/10475) Correctly disambiguate / after async fuctions ([@&#8203;nicolo-ribaudo](https://github.com/nicolo-ribaudo))
> * `babel-parser`, `babel-plugin-proposal-optional-chaining`, `babel-plugin-transform-modules-amd`
>   * [#10806](https://github-redirect.dependabot.com/babel/babel/pull/10806) fix(optional chaining): Optional delete returns true with nullish base ([@&#8203;mpaarating](https://github.com/mpaarating))
> * `babel-helper-module-transforms`, `babel-plugin-transform-modules-amd`
>   * [#10764](https://github-redirect.dependabot.com/babel/babel/pull/10764) fix: rewriteBindingInitVisitor should skip on scopable node ([@&#8203;JLHwung](https://github.com/JLHwung))
> 
> #### :nail_care: Polish
> * `babel-plugin-transform-runtime`
>   * [#10788](https://github-redirect.dependabot.com/babel/babel/pull/10788) Do not transpile typeof helper with itself in babel/runtime ([@&#8203;nicolo-ribaudo](https://github.com/nicolo-ribaudo))
> * `babel-core`
>   * [#10778](https://github-redirect.dependabot.com/babel/babel/pull/10778) refactor: Improve error message in @babel/core ([@&#8203;jaroslav-kubicek](https://github.com/jaroslav-kubicek))
> 
> #### :house: Internal
> * `babel-preset-env-standalone`
>   * [#10779](https://github-redirect.dependabot.com/babel/babel/pull/10779) Bundle standalone using rollup ([@&#8203;JLHwung](https://github.com/JLHwung))
> * Other
>   * [#10781](https://github-redirect.dependabot.com/babel/babel/pull/10781) Tune makefile scripts ([@&#8203;JLHwung](https://github.com/JLHwung))
> * `babel-helper-transform-fixture-test-runner`
>   * [#10566](https://github-redirect.dependabot.com/babel/babel/pull/10566) Incorrect trace position in fixture runner ([@&#8203;JLHwung](https://github.com/JLHwung))
> 
> ## v7.7.4 (2019-11-23)
> 
> #### :bug: Bug Fix
> * `babel-runtime-corejs2`, `babel-runtime-corejs3`, `babel-runtime`
>   * [#10748](https://github-redirect.dependabot.com/babel/babel/pull/10748) Add support for native esm to @babel/runtime. ([@&#8203;nicolo-ribaudo](https://github.com/nicolo-ribaudo))
> * `babel-preset-env`
>   * [#10742](https://github-redirect.dependabot.com/babel/babel/pull/10742) Update preset-env mappings. ([@&#8203;existentialism](https://github.com/existentialism))
> * `babel-parser`
>   * [#10737](https://github-redirect.dependabot.com/babel/babel/pull/10737) Flow enums: fix enum body location. ([@&#8203;gkz](https://github.com/gkz))
>   * [#10657](https://github-redirect.dependabot.com/babel/babel/pull/10657) Fix some incorrect typeof parsing in flow. ([@&#8203;existentialism](https://github.com/existentialism))
>   * [#10582](https://github-redirect.dependabot.com/babel/babel/pull/10582) [parser] Allow optional async methods. ([@&#8203;gonzarodriguezt](https://github.com/gonzarodriguezt))
>   * [#10710](https://github-redirect.dependabot.com/babel/babel/pull/10710) register import equals specifier. ([@&#8203;JLHwung](https://github.com/JLHwung))
></tr></table> ... (truncated)
</details>
<details>
<summary>Commits</summary>

- [`f753c48`](https://github.com/babel/babel/commit/f753c48f74e9556265796806370fdf104e8147eb) v7.7.6
- [`4436ffd`](https://github.com/babel/babel/commit/4436ffd604912df589f34a368d57ef5c89acc6fe) Revert "Add ".js" extension to injected polyfill imports ([#10549](https://github-redirect.dependabot.com/babel/babel/issues/10549))" ([#10835](https://github-redirect.dependabot.com/babel/babel/issues/10835))
- [`655a972`](https://github.com/babel/babel/commit/655a9727379bc59c9619621e413eb41835e5dc7d) chore: add PR Revert labels to changelog ([#10836](https://github-redirect.dependabot.com/babel/babel/issues/10836))
- [`8a8474c`](https://github.com/babel/babel/commit/8a8474c321e358ffbdc5d8d9cef443a8526cf7b4) Add v7.7.5 to CHANGELOG.md [skip ci]
- [`d04508e`](https://github.com/babel/babel/commit/d04508e510abc624b3e423ff334eff47f297502a) v7.7.5
- [`d3a37b5`](https://github.com/babel/babel/commit/d3a37b5d08074c007268eb15a11ae432165bad5d) Add ".js" extension to injected polyfill imports ([#10549](https://github-redirect.dependabot.com/babel/babel/issues/10549))
- [`c9a6898`](https://github.com/babel/babel/commit/c9a68984d6e727b4c326fc9ad55caa8d54fbea29) `babel --watch` should have equivalent file selection logic with `babel` ([#10](https://github-redirect.dependabot.com/babel/babel/issues/10)...
- [`c6e966c`](https://github.com/babel/babel/commit/c6e966cac95f5fb415984af430e9f1a153ec3078) [parser] Use scope flags to check arguments ([#10801](https://github-redirect.dependabot.com/babel/babel/issues/10801))
- [`a0bed42`](https://github.com/babel/babel/commit/a0bed42aec00196e0611df3cbb2ec6ebc36fbec6) Bundle standalone using rollup ([#10779](https://github-redirect.dependabot.com/babel/babel/issues/10779))
- [`2b47291`](https://github.com/babel/babel/commit/2b472912e4a0f0994193d07a476512cc47ffdece) Tune makefile scripts ([#10781](https://github-redirect.dependabot.com/babel/babel/issues/10781))
- Additional commits viewable in [compare view](https://github.com/babel/babel/compare/v7.6.3...v7.7.6)
</details>
<br />

[![Dependabot compatibility score](https://api.dependabot.com/badges/compatibility_score?dependency-name=@babel/preset-env&package-manager=npm_and_yarn&previous-version=7.6.3&new-version=7.7.6)](https://dependabot.com/compatibility-score.html?dependency-name=@babel/preset-env&package-manager=npm_and_yarn&previous-version=7.6.3&new-version=7.7.6)

Dependabot will resolve any conflicts with this PR as long as you don't alter it yourself. You can also trigger a rebase manually by commenting `@dependabot rebase`.

[//]: # (dependabot-automerge-start)
[//]: # (dependabot-automerge-end)

---

<details>
<summary>Dependabot commands and options</summary>
<br />

You can trigger Dependabot actions by commenting on this PR:
- `@dependabot rebase` will rebase this PR
- `@dependabot recreate` will recreate this PR, overwriting any edits that have been made to it
- `@dependabot merge` will merge this PR after your CI passes on it
- `@dependabot squash and merge` will squash and merge this PR after your CI passes on it
- `@dependabot cancel merge` will cancel a previously requested merge and block automerging
- `@dependabot reopen` will reopen this PR if it is closed
- `@dependabot close` will close this PR and stop Dependabot recreating it. You can achieve the same result by closing it manually
- `@dependabot badge me` will comment on this PR with code to add a "Dependabot enabled" badge to your readme

Additionally, you can set the following in the `.dependabot/config.yml` file in this repo:
- Update frequency
- Automerge options (never/patch/minor, and dev/runtime dependencies)
- Out-of-range updates (receive only lockfile updates, if desired)
- Security updates (receive only security updates, if desired)



</details>